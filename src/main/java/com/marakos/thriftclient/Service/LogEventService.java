package com.marakos.thriftclient.Service;

import com.marakos.thriftclient.com.thrift.impl.LogEvent;
import org.springframework.stereotype.Service;

@Service
public interface LogEventService {

    /*
    Generates pseudo-random LogEvents
     */
    LogEvent generateRandomLogEvent();
}
