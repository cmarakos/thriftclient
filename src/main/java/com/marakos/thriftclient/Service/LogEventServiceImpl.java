package com.marakos.thriftclient.Service;

import com.marakos.thriftclient.com.thrift.impl.LogEvent;
import com.marakos.thriftclient.com.thrift.impl.ThriftTimestamp;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class LogEventServiceImpl implements LogEventService {

    List<String> levelList = Arrays.asList( "FATAL","ERROR","WARN","INFO","DEBUG");
    List<String> browserList = Arrays.asList( "Chrome","Opera","Firefox","Brave");
    List<String> userList = Arrays.asList( "Bob","Alice","Cons");
    Random rand = new Random();

    @Override
    public LogEvent generateRandomLogEvent() {
        Date now = new Date();
        ThriftTimestamp timestamp = new ThriftTimestamp(now.getYear(), now.getMonth(), now.getDay(), now.getHours(), now.getMinutes(),now.getSeconds());
        return new LogEvent(
                (short)1,
                timestamp,
                "This is a nice event message at "+now.getTime(),
                levelList.get(rand.nextInt(levelList.size())),
                browserList.get(rand.nextInt(browserList.size())),
                userList.get(rand.nextInt(userList.size()))
        );
    }
}
