package com.marakos.thriftclient.Controller;

import com.marakos.thriftclient.Service.LogEventService;
import com.marakos.thriftclient.com.thrift.impl.Message;
import com.marakos.thriftclient.com.thrift.impl.ThriftService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;


@Controller
public class LogEventController {

    @Value("${thrift.socket.host}")
    private String host;

    @Value("${thrift.socket.port}")
    private Integer port;

    @Autowired
    LogEventService logEventService;

    Logger logger = LogManager.getLogger(LogEventController.class);

    /*
    Opens connection to the socket specified at port given and start sending messages
     */
    @EventListener(ApplicationReadyEvent.class)
    public void init() throws TException, InterruptedException {
        TTransport transport = new TSocket(host, port);
        transport.open();
        TProtocol protocol = new TBinaryProtocol(transport);

        ThriftService.Client client = new ThriftService.Client(protocol);

        int i=0;
        try {
            do{

                Message message = new Message((short)i,logEventService.generateRandomLogEvent());
                client.sendLogEvent(message);
                logger.info("Sent message with id:" +i);
                Thread.sleep(1000);
                i++;
            }while (true);
        }catch (Exception e){
            logger.info(e.getMessage());
        }finally {
            transport.close();
        }
    }
}
