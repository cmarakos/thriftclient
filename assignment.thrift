namespace java com.marakos

exception InvalidOperationException {
    1: i32 code,
    2: string description
}

struct ThriftTimestamp{
     1: i32 year;
     2: i32 month;
     3: i32 day;
     4: i32 hour;
     5: i32 minute;
     6: i32 second;
}

struct LogEvent{
    1: i16 version;
    2: ThriftTimestamp time;
    3: string message;
    4: string level;
    5: string browser;
    6: string user;
}

struct Message {
1: i16 id;
2: LogEvent logEvent;
}

service ThriftService {
    oneway void sendLogEvent(1:Message message);

}